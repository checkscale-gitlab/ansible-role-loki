ansible-role-loki
=========

Create Loki config files. Use in combination with [this Loki docker setup](https://gitlab.com/naturalis/core/analytics/docker-compose-loki).

Requirements
------------
* Ansible >= 2.7


Role inputs requirement
--------------

| Name           | Default value | Description                        |
| -------------- | ------------- | -----------------------------------|
| `loki_aws_access_key_id` | none | AWS S3 Bucket access key. |
| `loki_aws_secret_access_key` | none | AWS S3 Bucket secret. |
| `loki_bucket_name` | node | AWS S3 Bucket name for loki chunks. |

Policy for s3 bucket user:
Actions:
  - s3:PutObject
  - s3:GetObject
  - s3:ListBucket
  - s3:DeleteObject
Resources:
  - arn:aws:s3:::<bucket_name>/*
  - arn:aws:s3:::<bucket_name>"

Role Variables
--------------

| Name           | Default value | Description                        |
| -------------- | ------------- | -----------------------------------|
| `loki_config_dir` | '/opt/compose_projects/loki/config' | Config directory. |

Dependencies
------------

* https://gitlab.com/naturalis/core/analytics/docker-compose-loki


Example Playbook
----------------

```yaml
- name: Apply loki role
  include_role:
    name: loki
    apply:
      tags: loki
  tags: always
```

License
-------
Apache License 2.0

Author Information
----------------
* Rudi Broekhuizen
